#!/bin/bash
sudo yum update -y

# git
# sudo yum install -y git
# git clone https://gitlab.com/lightingblade55/simple-flask-app.git

#pip
curl -O https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py --user

#docker
sudo yum install -y docker
sudo usermod -a -G docker ec2-user
id ec2-user
# newgrp docker
sudo systemctl enable docker.service
sudo systemctl start docker.service

# docker compose
wget https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) 
sudo mv docker-compose-$(uname -s)-$(uname -m) /usr/local/bin/docker-compose
sudo chmod -v +x /usr/local/bin/docker-compose

#kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
kubectl version --client

#minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

minikube config set cpus 4
minikube config set memory 16GB
minikube config set disk-size 100GB
minikube start --force
